class FreeclimbApp < Sinatra::Base

  require './db.rb'
  require './helpers/app.rb'
  helpers ApplicationHelpers

  enable :sessions
  use Rack::Flash
  use Rack::MethodOverride

  before do
    @request = request
    protected! if @request.path =~ /^\/(posts)|(delete)/
  end

  get '/photos', {} { haml :photos }

  # Default top-level catch-all
  get /^\/[^\/]*$/ do
    #    flash[:error] = 'This is an error'
    @posts = Post.all(:order => [:created_at.desc]).
      paginate(:page => params[:page], :per_page => 3)
    haml :'posts/index'
  end

  # Generic delete routes
  get '/delete/:type/:id/?:redirect_path?' do |type, id, redirect_path|
    @type = type
    @id = id
    @redirect_path = redirect_path || '/'
    haml :delete
  end
  delete '/delete/:type/:id/?:redirect_path?' do |type, id, redirect_path|
    flash[:notice] = 'Element deleted!'
    model = Object.const_get(type)
    obj = model.get(id)
    obj.destroy
    redirect redirect_path
  end

  # Routes for handling basic operations on Posts
  get '/posts/new', {} { haml :'posts/new' }
  get '/posts/edit/:id' do |id|
    @post = Post.get(id)
    haml :'posts/edit' 
  end

  post '/posts/create' do 
    flash[:notice] = "Post saved!"
    @post = Post.create(params[:post])
    haml :'posts/show'
  end

end
