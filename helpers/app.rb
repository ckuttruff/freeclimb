module ApplicationHelpers
  require 'will_paginate/data_mapper'
  require 'rack-flash'
  include WillPaginate::Sinatra::Helpers

  def include_js(*files)
    files.each { |f| haml_tag :script, :src => f, :type => 'text/javascript' }
  end

  def include_css(*files)
    files.each { |f| haml_tag :link, :rel => 'stylesheet', :href => f, :type => 'text/css' }
  end

  def nav_link(name, link_path, request_path, icon = '')
    class_str = (request_path == link_path ? 'active' : '')
    haml_tag :li, :class => class_str do 
      icon = "<i class='#{icon}'></i>" unless icon.empty?
      haml_tag :a, (icon + name), :href => link_path 
    end
  end

  def partial(template, locals = {})
    haml(template, :layout => false, :locals => locals)
  end

  def form(model_name, path, form_inputs, defaults = {})
    @model_name = model_name
    @path = path
    @form_inputs = form_inputs
    @defaults = defaults

    haml :form
  end

  # Right now just handling '/' for redirect paths, but will expand this later
  #  when necessary
  def url_escape(str)
    str.gsub(/\//, '%2F')
  end

  # Authentication related code
  def protected!
    unless authorized?
      response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
      throw(:halt, [401, "Not authorized\n"])
    end
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    (@auth.provided? && @auth.basic? && @auth.credentials &&
     @auth.credentials == ['admin', ENV['SINATRA_AUTH_PW']])
  end

end
