class Post
  include DataMapper::Resource

  property :id,         Serial
  property :title,      Text,     :required => true, :lazy => false
  property :body,       Text,     :required => true
  property :created_at, DateTime

end
