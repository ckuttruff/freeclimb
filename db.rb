require 'data_mapper'
require 'dm-postgres-adapter'
require 'dm-migrations'

# Use logging for now
DataMapper::Logger.new($stdout, :debug)

# Initialize connection
db_url = ENV['DATABASE_URL'] || 'postgres://freeclimb:freeclimb@localhost/freeclimb_dev'
DataMapper.setup(:default, db_url)

# Load our models
Dir.glob('models/*.rb').each { |d| require "./#{d}" }

DataMapper.finalize
DataMapper.auto_upgrade!

